#!/usr/bin/perl
# vim: set et ts=4 sw=4:

#*****************************************************************************
# 
#  Copyright (c) 2014 Matteo Pasotti <matteo.pasotti@gmail.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License version 2, as
#  published by the Free Software Foundation.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#*****************************************************************************
#

package CF::Calculator;

use Modern::Perl '2011';
use autodie;
use Moose;

use CF::Comuni;

has 'consonanti' => (
	is => 'rw',
	isa => 'ArrayRef',
	init_arg => undef,
	builder => '_buildConsonanti'
);

has 'vocali' => (
	is => 'rw',
	isa => 'ArrayRef',
	init_arg => undef,
	builder => '_buildVocali',
);

has 'mesi' => (
	is => 'rw',
	isa => 'HashRef',
	init_arg => undef,
	builder => '_buildMesi',
);


has 'pari' => (
	is => 'rw',
	isa => 'HashRef',
	init_arg => undef,
	builder => '_buildPari',
);

has 'dispari' => (
	is => 'rw',
	isa => 'HashRef',
	init_arg => undef,
	builder => '_buildDispari',
);

has 'controllo' => (
	is => 'rw',
	isa => 'HashRef',
	init_arg => undef,
	builder => '_buildControllo',
);

has 'dateSeparator' => (
	is => 'rw',
	init_arg => undef,
	default => "/",
);

sub _buildConsonanti {
	my $self = shift();

	my @cons = (
		'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K',
		'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T',
		'V', 'W', 'X', 'Y', 'Z'
		);
	return \@cons;
}

sub _buildVocali {
	my $self = shift();

	my @vocali = (
		'A', 'E', 'I', 'O', 'U'
		);
	return \@vocali;
}

sub _buildMesi {
	my $self = shift();

	my %mesi = (
	  	1 => 'A',
		2 => 'B',
       	3 => 'C',
       	4 => 'D',
		5 => 'E',
       	6 => 'H',
		7 => 'L',
		8 => 'M',
		9 => 'P',
		10 => 'R', 
		11 => 'S',
		12 => 'T',
		);
	return \%mesi;
}

sub _buildPari {
	my $self = shift();

	my %v = (
		'0' =>  0, '1' =>  1, '2' =>  2, '3' =>  3, '4' =>  4, 
        '5' =>  5, '6' =>  6, '7' =>  7, '8' =>  8, '9' =>  9,
        'A' =>  0, 'B' =>  1, 'C' =>  2, 'D' =>  3, 'E' =>  4, 
        'F' =>  5, 'G' =>  6, 'H' =>  7, 'I' =>  8, 'J' =>  9,
        'K' => 10, 'L' => 11, 'M' => 12, 'N' => 13, 'O' => 14, 
        'P' => 15, 'Q' => 16, 'R' => 17, 'S' => 18, 'T' => 19,
        'U' => 20, 'V' => 21, 'W' => 22, 'X' => 23, 'Y' => 24, 
        'Z' => 25,
	);
	return \%v;
}

sub _buildDispari {
	my $self = shift();

	my %v = (
		'0' =>  1, '1' =>  0, '2' =>  5, '3' =>  7, '4' =>  9,
        '5' => 13, '6' => 15, '7' => 17, '8' => 19, '9' => 21,
        'A' =>  1, 'B' =>  0, 'C' =>  5, 'D' =>  7, 'E' =>  9, 
        'F' => 13, 'G' => 15, 'H' => 17, 'I' => 19, 'J' => 21,
        'K' =>  2, 'L' =>  4, 'M' => 18, 'N' => 20, 'O' => 11, 
        'P' =>  3, 'Q' =>  6, 'R' =>  8, 'S' => 12, 'T' => 14,
        'U' => 16, 'V' => 10, 'W' => 22, 'X' => 25, 'Y' => 24, 
        'Z' => 23
	);
	return \%v;
}

sub _buildControllo {
	my $self = shift();	

	my %v = (
		'0'  => 'A', '1'  => 'B', '2'  => 'C', '3'  => 'D', 
        '4'  => 'E', '5'  => 'F', '6'  => 'G', '7'  => 'H', 
        '8'  => 'I', '9'  => 'J', '10' => 'K', '11' => 'L', 
        '12' => 'M', '13' => 'N', '14' => 'O', '15' => 'P', 
        '16' => 'Q', '17' => 'R', '18' => 'S', '19' => 'T',
        '20' => 'U', '21' => 'V', '22' => 'W', '23' => 'X', 
        '24' => 'Y', '25' => 'Z'
	);
	return \%v;
}

sub getLettere {
	my $self = shift(); 
	my $string = shift();
	my $haystack = shift();
    my @letters = ();
    for my $needle(split('',$string)) {
        if( grep {$needle eq $_} @$haystack )
		{
            push(@letters, $needle);
        }
    }
    return @letters;
}

sub getVocali {
	my $self = shift();
	my $string = shift();
	return $self->getLettere($string,$self->vocali);
}

sub getConsonanti {
	my $self = shift();
	my $string = shift();
	return $self->getLettere($string,$self->consonanti);
}

sub sanitize {
	my $self = shift();
	my $string = shift();
	my $toupper = shift();
	$toupper = 1 if(!defined($toupper));
    	my $result = $string;
	$result =~s/[^A-Za-z]*//g;
	if($toupper)
	{
		return uc($result);
	}
	return $result;
}

sub addMissingX {
	my $self = shift();
	my $string = shift();
	my $code = $string;
    while(length($code) < 3) {
        $code .= 'X';
    }    
    return $code;
}

sub calcName {
	my $self = shift();
	my $string = shift();
	my $nome = $self->sanitize($string);
	my $code = "";

    # Se il nome inserito e' piu' corto di 3 lettere
    # si aggiungono tante X quanti sono i caratteri
    # mancanti.
    if (length($nome) < 3) {
        return $self->addMissingX($nome);
    } 
        
    my @nome_cons = $self->getConsonanti($nome);

    # Se le consonanti contenute nel nome sono minori 
    # o uguali a 3 vengono considerate nell'ordine in cui
    # compaiono.
    if (scalar(@nome_cons) <= 3) {
        $code = join('', @nome_cons);
    } else {
        # Se invece abbiamo almeno 4 consonanti, prendiamo
        # la prima, la terza e la quarta.
        for(my $i=0; $i<4; $i++) {
            next if ($i == 1);
            if ( length(AdminPanel::Shared::trim($nome_cons[$i])) > 0 ) {
                $code .= $nome_cons[$i];
            }
        }
    }
    
    # Se compaiono meno di 3 consonanti nel nome, si
    # utilizzano le vocali, nell'ordine in cui compaiono
    # nel nome.
    if (length($code) < 3) {
        my @nome_voc = $self->getVocali($nome);
        while (length($code) < 3) {
           $code .= shift(@nome_voc); 
        }
    }
    
    return $code;
}

sub calcSurname {
	my $self = shift();
	my $string = shift();
	my $cognome = $self->sanitize($string);
    my $code = "";
    
	# Se il cognome inserito e' piu' corto di 3 lettere
	# si aggiungono tante X quanti sono i caratteri
	# mancanti.
	if (length($cognome) < 3) {
		return $self->addMissingX($cognome);
	}
	my @cognome_cons = $self->getConsonanti($cognome);

	# Per il calcolo del cognome si prendono le prime
	# 3 consonanti. 
	for (my $i=0; $i<3; $i++) {
		if (defined($cognome_cons[$i])) {
			$code .= $cognome_cons[$i];
		}
	}

	# Se le consonanti non bastano, vengono prese
	# le vocali nell'ordine in cui compaiono.
	if (length($code) < 3) {
		my @cognome_voc = $self->getVocali($cognome);
		while (length($code) < 3) {
			$code .= shift(@cognome_voc);
		}
	}
	return $code;   
}

sub calcolaDataNascita {
	my $self = shift();
	my $data = shift();
	my $sesso = shift();
    
	my $dn = split($self->dateSeparator, $data); 

	my $giorno = undef;
	my $mese = undef;
	my $anno = undef;

    my $sep = $self->dateSeparator;
	if($data =~m/(.+)$sep(.+)$sep(.+)/)
	{
		$giorno = $1;
		$mese   = $2;
		$anno   = $3;
	}
    
    # Le ultime due cifre dell'anno di nascita
    my $aa = substr($anno, -2);
    
	# La lettera corrispondente al mese di nascita
    $mese =~s/^0//g;
    my $mm = $self->mesi->{"$mese"};

    # Il giorno viene calcolato a seconda del sesso
    # del soggetto di cui si calcola il codice:
    # se e' Maschio si mette il giorno reale, se e' 
    # Femmina viene aggiungo 40 a questo numero.
    my $gg = (uc($sesso) eq 'M') ? $giorno : ($giorno + 40);

    # Bug #1: Thanks to Luca 
    $gg = '0' . $gg if (length($gg) < 2);


    return $aa . $mm . $gg;        
}

sub calcolaCifraControllo {
	my $self = shift();
	my $codice = shift();
	
	my @code = split(//,$codice);
    my $sum  = 0;

	my $cifra = undef;

    for(my $i=1; $i <= scalar(@code); $i++) {
        $cifra = $code[$i-1];
		if($i % 2)
		{
			$sum += $self->dispari->{$cifra};
		}        
		else
		{
			$sum += $self->pari->{$cifra};
		}
    }

    $sum = $sum % 26;

    return $self->controllo->{$sum};
}

sub calculate {
	my $self = shift();
	my $nome = shift();
	my $cognome = shift();
	my $data = shift();
	my $sesso = shift();
	my $comune = shift();
	my $provincia = shift();
    my $cf_comuni = CF::Comuni->new();
    my $codice = $self->calcSurname($cognome) . 
              $self->calcName($nome) . 
              $self->calcolaDataNascita($data, $sesso) . 
              $cf_comuni->getCodCatastaleByComuneProvincia($comune, $provincia);
   
    $codice .= $self->calcolaCifraControllo($codice);

    if (length($codice) != 16) {
        return 0;
    }

    return $codice;
} 

1;
