#!/usr/bin/perl
# vim: set et ts=4 sw=4:

#*****************************************************************************
# 
#  Copyright (c) 2014 Matteo Pasotti <matteo.pasotti@gmail.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License version 2, as
#  published by the Free Software Foundation.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#*****************************************************************************
#

package CF::Comuni;

use Modern::Perl '2011';
use autodie;
use Moose;

use List::MoreUtils qw(uniq);

has 'dbCatastale' => (
    is      => 'rw',
    isa     => 'Str',
    default => 'db_cf.csv'
);

sub getCodCatastaleByComuneProvincia {
	my $self = shift();
	my $comune = shift();
	my $provincia = shift();
	
	my $fh = undef;
	my @items = undef;
	my $result = undef;

	open($fh, "<", $self->dbCatastale) || die("Cannot open the db\n");
	while(<$fh>)
	{
		# 0th item -> code
		# 2nd item -> provincia
		# 3rd item -> comune
		@items = split(/,/,$_);
		if((AdminPanel::Shared::trim($items[2]) eq $provincia)&&
			(AdminPanel::Shared::trim($items[3]) eq $comune))
		{
			$result = $items[0];
			last;
		}
	}
	close($fh);
	return $result;
}

sub getListComuni {
    my $self = shift();
    my $fh = undef;
	my @items = undef;

	my @result = ();

	open($fh, "<", $self->dbCatastale) || die("Cannot open the db\n");
    my $dropFirstLine = <$fh>;
	while(<$fh>)
	{
		# 0th item -> code
		# 2nd item -> provincia
		# 3rd item -> comune
		@items = split(/,/,$_);
        push(@result, $items[3]);
	}
	close($fh);
    @result = sort(uniq(@result));
    return @result;
}

sub getProvinceByComune {
    my $self = shift();
	my $comune = shift();
	
	my $fh = undef;
	my @items = undef;
	my @result = ();

	open($fh, "<", $self->dbCatastale) || die("Cannot open the db\n");
    my $dropFirstLine = <$fh>;
	while(<$fh>)
	{
		# 0th item -> code
		# 2nd item -> provincia
		# 3rd item -> comune
		@items = split(/,/,$_);
		if(AdminPanel::Shared::trim($items[3]) eq AdminPanel::Shared::trim($comune))
		{
			push(@result, $items[2]);
		}
	}
	close($fh);
    @result = uniq(@result);
	return @result;
}

1;
