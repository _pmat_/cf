# vim: set et ts=4 sw=4:
#*****************************************************************************
# 
#  Copyright (c) 2014 Matteo Pasotti <matteo.pasotti@gmail.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License version 2, as
#  published by the Free Software Foundation.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#*****************************************************************************

package CF::GUI;
 
use Modern::Perl '2011';
use autodie;
use Moose;
use POSIX qw(ceil);
use utf8;

use yui;
use AdminPanel::Shared qw(trim);
use AdminPanel::Shared::Locales;
use CF::Calculator;
use CF::Comuni;


=head1 VERSION

Version 1.0.0

=cut

our $VERSION = '1.0.0';

has 'dialog' => (
    is => 'rw',
    init_arg => undef
);

has 'loc' => (
        is => 'rw',
        init_arg => undef,
        builder => '_localeInitialize'
);

sub _localeInitialize {
    my $self = shift();

    # TODO fix domain binding for translation
    $self->loc(AdminPanel::Shared::Locales->new(domain_name => 'cf') );
    # TODO if we want to give the opportunity to test locally add dir_name => 'path'
}

#=============================================================
sub start {
    my $self = shift;

    $self->_mainDialog();
};


sub populateCombBoxComuni {
    my $self = shift();
    my $cbo = shift();
    my $cf_comuni = CF::Comuni->new();
    my @lista = $cf_comuni->getListComuni();
    my $itemColl = new yui::YItemCollection();
    for(@lista)
    {
        my $item = new yui::YItem($_,0);
        $itemColl->push($item);
        $item->DISOWN();
    }
    $cbo->addItems($itemColl);
}

sub _mainDialog {
    my $self = shift;

    my $newTitle = $self->loc->N("Calcola il Codice Fiscale");
    yui::YUI::app()->setApplicationTitle($newTitle);

    my $factory  = yui::YUI::widgetFactory;
    my $optional = yui::YUI::optionalWidgetFactory;
    

    $self->dialog($factory->createMainDialog());
    my $layout    = $factory->createVBox($self->dialog);

    my $hbox_header = $factory->createHBox($layout);
    my $headLeft = $factory->createHBox($factory->createLeft($hbox_header));
    my $headRight = $factory->createHBox($factory->createRight($hbox_header));

    #my $logoImage = $factory->createImage($headLeft, $appIcon);
    my $labelAppDescription = $factory->createLabel($headRight,$newTitle); 
    #$logoImage->setWeight($yui::YD_HORIZ,0);
    $labelAppDescription->setWeight($yui::YD_HORIZ,3);

    my $hbox_content = $factory->createHBox($layout);

    my $inputBox = $factory->createVBox($hbox_content);
    my $cognome = $factory->createInputField($factory->createHBox($inputBox), "Cognome");
    my $nome = $factory->createInputField($factory->createHBox($inputBox), "Nome");
    my $comune = $factory->createComboBox($factory->createHBox($inputBox), "Comune di Nascita");
    my $provincia = $factory->createComboBox($factory->createHBox($inputBox), "Provincia");
    my $nascita = $factory->createInputField($factory->createHBox($inputBox), "Data di Nascita (dd/mm/yyyy)");
    my $sex_label = $factory->createLabel($factory->createHBox($inputBox), "Sesso");
    my $sex = $factory->createRadioButtonGroup($factory->createHBox($inputBox));
    my $sexbox = $factory->createVBox($sex);
    my $men = $factory->createRadioButton($sexbox, 'M');
    my $women = $factory->createRadioButton($sexbox, 'F');
    $men->setValue(1);
    my $cfOutput = $factory->createInputField($factory->createHBox($inputBox), "Generato");

    $comune->setNotify(1);

    $self->populateCombBoxComuni($comune);

    $cognome->setWeight($yui::YD_HORIZ,3);
    $nome->setWeight($yui::YD_HORIZ,3);
    $comune->setWeight($yui::YD_HORIZ,3);
    $provincia->setWeight($yui::YD_HORIZ,3);
    $nascita->setWeight($yui::YD_HORIZ,3);
    $sex->setWeight($yui::YD_HORIZ,3);
    $cfOutput->setWeight($yui::YD_HORIZ,3);

    my $buttonPanel = $factory->createVBox($hbox_content);
    my $calcButton = $factory->createPushButton($factory->createHBox($buttonPanel),$self->loc->N("Calculate"));
    my $printButton = $factory->createPushButton($factory->createHBox($buttonPanel),$self->loc->N("Print"));
    my $cancelButton = $factory->createPushButton($factory->createHBox($buttonPanel),$self->loc->N("Cancel"));
    $calcButton->setWeight($yui::YD_HORIZ,1);
    $printButton->setWeight($yui::YD_HORIZ,1);
    $cancelButton->setWeight($yui::YD_HORIZ,1);

    # main loop
    while(1) {
        my $event     = $self->dialog->waitForEvent();
        my $eventType = $event->eventType();
        
        #event type checking
        if ($eventType == $yui::YEvent::CancelEvent) {
            last;
        }
        elsif ($eventType == $yui::YEvent::WidgetEvent) {
### Buttons and widgets ###
            my $widget = $event->widget();
            if ($widget == $cancelButton) {
                last;
            }
            elsif ($widget == $calcButton) {
                my $calculator = CF::Calculator->new();
                next if(AdminPanel::Shared::trim($nascita->value()) eq "");
                my $sex_value = $sex->currentButton()->label();
                $sex_value =~s/\&//g;
                $cfOutput->setValue($calculator->calculate($nome->value(),$cognome->value(),$nascita->value(),$sex_value,$comune->value(),$provincia->value()));
            }
            elsif ($widget == $comune) {
                my $cf_comuni = CF::Comuni->new();
                my @province = $cf_comuni->getProvinceByComune($comune->value());
                my $itemColl = new yui::YItemCollection();
                for my $prov(@province)
                {
                    my $item = new yui::YItem($prov,0);
                    $itemColl->push($item);
                    $item->DISOWN();
                }
                $provincia->deleteAllItems();
                $provincia->addItems($itemColl);
            }
        }
    }

    $self->dialog->destroy() ;

}

1;
